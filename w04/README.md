Task 1:
1. Added called to empty.wait(), empty.post(), filled.wait(), and filled.post() to the producer and consumer functions and linked code with semaphore-ar.obj. The result produces a the correct ordering.

Task 2: Skipped (Optional)

Task 3:
1. List the variables that are subject to race condition and describe the potential side effect of race condition.
Answer: The empty and filled variables are subject to the race condition since two different threads could access these variables at the same time and overwrite the products that are added to the buffer. If two producers are trying to access the same slot, only one of the numbers will be listed.

2. Try the following shell command and explain the output.
Answer: The output of the for loop is a table indicating the number of times each slot in the buffer was accessed. Also there are products missing in the list since there are race conditions. Furthermore, the slots are accessed inconsistently because there is not control of what threads are dealing with what product. 

3. Added locks to pc.cc to protect the critical sections of the code to avoid race condition. The code works correctly after these changes.

Submitted pc.cc and semaphore.cc as well.
