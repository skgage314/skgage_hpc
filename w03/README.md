Task 1:
1. Copied the makefile contents given into the makefile and compiled the code with the command make. 
2. When you type the command "make clean" on the command line, it executes the action after "clean:". In this case, it removes all object files, executables, and the output file.

Task 2: 
2. Modified the makefile and used varaibles for the compiler and its flags.

Task 3:
3. Printed when calling make: 
g++  -I. -c -o main.o main.cc
g++  -I. -c -o message.o message.cc
g++ -I. -o output main.o message.o

4. After changing the messsage.cc file and calling "make" again, the following is printed:
g++  -I. -c -o message.o message.cc
g++ -I. -o output main.o message.o

Only files that were changed need to recompiled, otherwise it is a waste to recompile all files again. Say there are a large number of files and only one is changed, then only update the object and executable files that need to be reupdated.

Task 4:
5. Before the return statement for fib(n), I added #pragma omp taskwait in order to return the correct answer. The taskwait directive specifies a wait on the completion of child tasks generated since the beginning of the current task. Since we require knowing the result of f(n-1) and f(n-2) before we can know fib(n), this directive is necessary to add.


Task 5:
1. Outline of strategy for quicksort using OpenMP in pseudo-code or plain English.
Answer: As done with the fibanacci code, I used tasks for each half of quicksort to work on sorting. For the partition portion of the algorithm, if an element of the vector is less than the pivot value, then that index in a new "less than" array is assigned a 1 and a 0 in a "greater than" array. If the element is greater, than the "greater than" array recieves a 1. This is done in a parallel for loop. Then a cumulative sum or prefex scan on each array to help decide where each index should go in the full array. 

2. Submitted quicksort code and makefile, see makefile2 and job.sh.

3. Reported scaling averaged between five runs: please see images "weak_scale_quicksort_plot" and "strong_scale_quicksort_plot". The strong scaling graph showed a speedup. 

4. Reason about performance expectation of code.
Answer: There are still places in my code that are running inefficiently, mostly in the partition section. After assigning binary identification for each index in the vector, I am not currently doing the cumulative sum to find the ordering of elements parallelly. With more time, I would look to improve the speedup more.


Task 6:
6. 1. Given A = n x n matrix, to multiply A*A takes n^3 multiplications and n^3-n^2 additions. Multiplication of two indices of the matrices is done before additions can be done. This being said, since there are n^3 processors, all the multiplications can be done first with Depth O(1) and Work O(n^3). Then a tree of additions with Depth O(log n) and Work O(n^3-n^2). 
2. When there are p = 2^q < n^3 processors, then the multiplication (which will still happen first) requires Depth O(n/p) and Work O(n^3). Then the additions require Depth O((n-m)/p+log(m)) for m = p and Work O(n^3-n^2).

7. 1. When A is not sorted, then with n processors, each processor can compare each value of A with the given x in Work O(n) and Depth O(1) in binary. If A[i] <= x, then assigned a value of 1. Then the adding up process of all 1's takes O(log n) time. 
2. If A is sorted, then we could compare each pair of two indices with a processor, taking O(n/2). The processor that holds the number returns it's index. And since A is sorted, then the value of the index that stores x is the rank #. An EREW PRAM model used.

