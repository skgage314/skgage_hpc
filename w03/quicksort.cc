
#include <omp.h>
#include <stdio.h>
#include <iostream>
#include <cstddef>
#include <vector>
#include <algorithm> //std::min_element, std: max_element
#include <iterator> //begin and end
#include <cmath>
#include <math.h>

typedef std::vector<long> vec_t;

void swap(vec_t &x, int i, int j) {
	//printf("Do I get to here? swap function");
	int temp = x[i];
	x[i] = x[j];
	x[j] = temp;
}

int sum(int a, int b) {
	return a + b;
}

void scan(vec_t &x) {
	int n = x.size();
	vec_t y(n);
	y[0] = x[0];
	//#pragma omp parallel for
	for (int i = 1; i<n; i++) {
		y[i] = sum(x[i], y[i-1]);
	}
	x = y;
}



int partition_func(vec_t &x, int low, int high) {
	int p = x[high];
	//int median_indice = high;
	//printf("pivot value = %d", p);
	int i = (low - 1);
	int n = high - low;
	vec_t less(n);// = {0};
	vec_t more(n);// = {0};
	vec_t acc_less(n);
	vec_t acc_more(n);
	//int n = high - low;
	int less_i = 0;
	int more_i = 0;
	int bn = 0;
	vec_t y(x.size());
	y = x;
	
	#pragma omp parallel for 
	for (int j = 0; j < n; j++) {
		//#pragma critical	
		if (x[low+j] <= p) {
		//	less[j] = 1;			
			less[j] = 1;
			//more[bn++] = 0;
//			less_i += 1;
//			printf("Do we get here less?");	
	//	i += 1;
		//	swap(x, i, j);
		}else {
//			more_i += 1;
			more[j] = 1;
//			printf("Do we get here more?");
			//less[more_i] = 0;
		}
	}
	acc_less[0] = less[0];
	if (less[0] == 1)
		less_i += 1;
//	#pragma omp parallel for
	for(int i = 1; i < n; i++) {
	//	#pragma critical
		if (less[i] == 1) 
			less_i += 1;
		acc_less[i] = sum(acc_less[i-1],less[i]);
	}
	y[low+less_i] = p;
	acc_more[0] = more[0];
	if (more[0] == 1)
		more_i += 1;
	//#pragma omp parallel for
	for (int j = 1; j < n; j++) {
	//	#pragma critical
		if (more[i] == 1) 
			more_i += 1;
		acc_more[j] = sum(acc_more[j-1],more[j]);
	}
	#pragma omp parallel for
	for (int i = 0; i < n; i++) {
		if (less[i] == 1) {
//			printf("\ni = %d, low+acc_less[i]-1 = %d and x[low+acc_less[i]-1] = %d\n", i, low+less_i+acc_more[i], x[low+i]);
			y[low+acc_less[i]-1] = x[low+i];
		} else {
//			printf("\ni = %d, low+less_i+acc_more[i] = %d and x[low+less_i+acc_more[i]] = %d\n", i, low+less_i+acc_more[i], x[low+i]);
			y[low+less_i+acc_more[i]] = x[low+i];
			}
	}
	y[low+less_i] = p;

	#pragma omp parallel for
	for (int j = 0; j <= n; j++) {
		x[low+j] = y[low+j];
	}
//	printf("Do we get here before returning?");
	return low + less_i;
//	swap(x, i+1, median_indice);
//	return i+1;
}

void quicksortHelper(vec_t &x, int low, int high) {
	if (low < high) {
		int p = partition_func(x, low, high);
	//	printf("pivot index is %d\n", p);
		#pragma omp parallel 
		#pragma omp single
		{
		#pragma omp task 
		quicksortHelper(x, low, p-1);
		#pragma omp task
		quicksortHelper(x, p+1, high);
		//#pragma omp taskwait
		}
	}	
	//}
}
void quicksort(const vec_t &x, vec_t &y) {
	int n;
	n = x.size();
        int tid; int sum;
        int n_threads;
        n_threads = omp_get_max_threads();
	quicksortHelper(y, 0, x.size()-1);
	//}
}
