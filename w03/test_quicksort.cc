/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 3 $
 * @tags $Tags:  $
 * @date $Date: Tue Sep 26 23:15:22 2017 -0600 $
 *
 */

#include "quicksort.h"
#include <iostream>
#include <cmath>
#include <cassert>
#include "omp-utils.h"
#include <algorithm>
#include <cstdio>

void rand_fill(vec_t &x){

    for (auto &el : x)
        el = rand() % 10;
}

int main(int argc, char** argv){

    // repeatable test
    srand(2017);

    // commandline arguments
    if (argc<2){
        printf("Array size is a mandatory arugments for %s.\n", __FILE__);
        exit(1);
    }

    size_t n(atoi(argv[1]));
    printf("Size of Problem = %d", n);
    printf("Sorting a random array of size %dK, num_threads=%d\n",n/1000,omp_get_max_threads());
    vec_t x(n),y(n);
    rand_fill(x);
    y = x;
	//    for (int i = 0; i < n; i++)
  //      std::cout << x[i] << "\n";
    double tic=NOW();
    quicksort(x,y);
    double toc=NOW();
 //   for (int i = 0; i < n; i++)
//	std::cout << y[i] << "\n";
    printf("Elapsed=%5.2e\n",toc-tic);

    // checking
    std::sort(x.begin(),x.end());
    for (size_t ii=0;ii<x.size();++ii) 
        assert (x[ii]==y[ii]);
    return 0;
}
