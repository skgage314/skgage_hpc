module load gcc

g++ -c -fopenmp fib.cc
g++ -fopenmp fib.o -o fib.exe

./fib.exe
