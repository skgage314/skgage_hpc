
#!/bin/bash

#SBATCH --time=0:05:00     # walltime, abbreviated by -t
#SBATCH --nodes=1          # number of cluster nodes, abbreviated by -N
#SBATCH -o histogram-%j.out     # name of the stdout redirection file, using the job$
#SBATCH -e histogram-%j.err     # name of the stderr redirection file
#SBATCH --ntasks 1      # number of parallel process
#SBATCH --qos debug        # quality of service/queue (See QOS section on CU RC$

# run the program
echo "Running on $(hostname --fqdn)"
module load gcc

g++ -c -fopenmp quicksort.cc
g++ -c -fopenmp test_quicksort.cc
g++ -fopenmp quicksort.o test_quicksort.o -o test_quicksort.exe

for i in {1..4}
do 
	OMP_NUM_THREADS=$i ./test_quicksort.exe $((4*10000))
done
