#include <omp.h>
#include <stdio.h>

long fib(long n) {
    long i(0), j(0);
    if (n<2) return 1;

    #pragma omp task shared(i)
    i = fib(n-1);
    printf("i = %d\n", i);

    #pragma omp task shared(j)
    j = fib(n-2);
    printf("j = %d\n", j);

    /* ... more code ... */
    #pragma omp taskwait
    return i + j;
}

int main(){

    long n(5), v;
    #pragma omp parallel shared (n, v)
    {
        #pragma omp single /* why single? */
        v = fib(n);
	printf("v = %d\n",v);
    }
	return v;
}

