Task 1:

1. The description for PATH on the man page for bash says that it is the search path for commands. It is the colon-separated list of directories in which the shell looks for commands. The zero-length directory name in the value of PATH indicates the current directory. A null directory name may appear as two adjacent colons, or as an initial or trailing colon. The default path is system-dependent, and is set by the administrator who installs bash.
2. What is the entry for LD_LIBRARY_PATH in the man page for ld? For a native linker, search the contents of the environment variable "LD_LIBRARY_PATH".
3. What is the value stored in HOME? You can use echo to show the value of the variable. /home/saga2465

Task 2:

4. Documentation read on the module system on CU RC.
5. Inspect the details of the default intel module on Summit. How does it change the PATH and LD_LIBRARY_PATH variables? using the module show intel command, the PATH and LD_LIBRARY_PATH variables are changed to: 
prepend_path("PATH","/curc/sw/gcc/5.4.0/bin")
prepend_path("LD_LIBRARY_PATH","/curc/sw/gcc/5.4.0/lib64")
and 
prepend_path("PATH","/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64")
prepend_path("LD_LIBRARY_PATH","/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64")
6. After you load the intel module, how does the list of available modules change? It says: 
"Currently loaded modules: 
	1) intel/17.4 (m)

	Where:
	m: built for host and native MIC"

7. Describe the motivation for hierarchical module system. The system provides five layers to support programs built with compiler and library consistency requirements. The user loads a compiler which extends the MODULEPATH to make available the modules that are built with the currently loaded compiler. 

Task 3:

8. What environment variables are set by the intel and gcc modules? (do module show <module>)
intel - 
pushenv("CC","icc")
pushenv("FC","ifort")
pushenv("CXX","icpc")
pushenv("AR","xiar")
pushenv("LD","xild")

gcc - 
pushenv("CC","gcc")
pushenv("FC","gfortran")
pushenv("CXX","g++")

Task 4:

9. What does ${CC} -v (where CC is either gcc or icc) print on your machine? (after installing GCC compiler on my machine) Configured with: --prefix=/Library/Developer/CommandLineTools/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
Apple LLVM version 9.0.0 (clang-900.0.39.2)
Target: x86_64-apple-darwin16.7.0
Thread model: posix
InstalledDir: /Library/Developer/CommandLineTools/usr/bin

10. What are the commands for compiling the object files and linking? 
To compile the object files: 
g++ -c test_axpy.cc and g++ -c axpy.cc
To link the object files to make an executable: 
g++ test_axpy.o axpy.o -o test_axpy

Task 5:

11. to run interactive job: sinteractive --qos=interactive --time=00:10:00 then could run sbatch job.sh
to check the job status of a job submitting on a compile node: squeue -u $USER
to view the job output: cat job-name.jobid.out
The contents of .out file is: Calling axpy - Passed
The contents of .err file is: *nothing*

Task 6:

12. The program only produces the "Calling axpy - Passed" without the proper flag. By adding the -fopenmp while compiling the contents of .out file is: 
Calling axpy - Passed
Elapsed time: 0.521013s

13. OMG_NUM_THREADS		ELAPSED TIME
    		1:  			0.785855s
    		2:  	 		0.653111s
    		4: 			0.440369s
    		8:  			0.311442s
    		16: 			0.271648s


