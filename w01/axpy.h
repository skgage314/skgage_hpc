/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 7 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief function delcration for axpy()
 */

#include <cstddef>
#include <stdexcept>

void axpy(size_t n, double a, const double *x, double *y) throw (std::runtime_error);

