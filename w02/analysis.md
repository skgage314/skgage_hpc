Task 1:

1. Describe in pseudo-code or plain English, the parallel algorithm and OpenMP features you are using in to implement histogram.
Answer: The steps to the serial algorithm are 1) find the max and min of the data set, 2) define the boundaries for each bin, and 3) compute number of elements in each of the bins. The parallel algorithm does the following for each of those steps identified. 1) A reduction max/min, 2) after computing the scale factor used ie (max-min)/#bin, use parallel for to map the max-min range to the bin range, and 3) parallel for and a) a critical region, meaning that there is a shared variable for the multiple threads or b) each thread has own count of number of elements in each bin, ie there is a private copy of the number of elements and then use a reduction sum to add counts from each thread for total sum //

2. Reason about the performance expectation of your code (apply the metrics we discussed in class).
Answer: Wall-clock time (T) - Actual time elapsed from the start of a program, Different from CPU time
T1(n) = O(n); Tp(n) = O(n/p); Speedup (S) - Ratio of the best sequential time T1 to the time on p processors Tp, S = p; Efficiency (E) - Speedup/p (less than 1); E = 1


Task 3:

1. Consider the problem of computing x^n, where n = 2^k for some non-negative integer k and x a scalar. The repeated-squaring algorithm consists of computing x^2, x^4 = x^2(x^2) and so on.
	a. Draw the DAG corresponding to this algorithm. What is the work and depth of this algorithm?
	Answer: DAG_scalar.png; As shown in example, at each level of the DAG, x^2^k is multipled by itself. Work is O(k) and depth O(k).
	b. Repeat for the case when x is an m x m matrix.
	Answer: DAG_matrix.png; For each vector dot product there are m multiplications and m-1 additions. And the matrix has m^2 elemnents, so the work is O(m^3) and depth of O(km). 

2. In the class, we discussed an the parallel sum (reduction) algorithm using the Work/Depth language model (for an array x of size n). State a PRAM algorithm with an arbitrary number of processors p <=n. Derive estimates for the work, time, speedup and efficiency as a function of the problem size n and the number of processors p.
Answer: Given problem size n and processors p<=n, Image PRAM.png shows an example of a PRAM algorithm for parallel sum (reduction) with n=4 and p=2. As you can see, if 2<p<=n then some of the processors are unneeded. If p<2 thenwe could split up the current algorithm into parts and solve. The work for this example and generally is O(n). Then since the depth of the tree is the log of the number of processors, Tp(n)=n/p+log(p). Then speedup S = T1/Tp = n/(n/p+log(p)). Finally E = S/p = n/(n+plog(p)). 

3. Suppose Tserial = n and Tparallel = n/p + log(p), where times are in microseconds. If we increase p by a factor of k, find a formula for how much we will need to increase n in order to maintain constant efficiency. Is the parallel program scalable?

        E = Ts/p(Tp) = n/(p(n/p+log(p))) = n/(n+plog(p)) = n/(n+log(p^p)) 
	if p <- kp so in order to maintain constant efficiency, then need to solve for x to find n <- xn in E = xn/(xn+kplog(kp)), returning x = klog(kp)/log(p), which reduces to x = k[logp(k)+1], so n <- k[logp(k)+1]n. And the parallel program is scalable. 


4. Considering the loop given. There is clearly a loop-carried dependence, as the value of a[i] cannot be computed without the value of a[i-1]. Suggest a way to eliminate this dependence and parallelize the loop.

	a[0] = 0;
	for (i = 1; i < n; ++i)
		a[i] = i(i+1)/2 [same as summation(j) with j = i, i-1, ..., 0]

	The above loop is parallelized and has the dependence eliminated. An alternate way of writing the original loop, as shown, is that the value at index i is equal to the summation of i + i-1 + i-2 + ... + 0, which is the same at i(i+1)/2. Each index i computation is independent of index i-1 unlike in the original loop.

         ex: solution [0, 1, 3, 6, 10]
produced by:
[0, 1+0, 2+1, 3+2+1, 4+3+2+1]
[i, i+i-1,...]
