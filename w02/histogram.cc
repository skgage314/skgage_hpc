/*
 * @input x data vector
 * @input num_bins the number of bins to use
 * @output bin_bdry vector of size num_bins+1 holding the left and
 *         right limit of each bin
 * @output bin_count array of size num_bins holding the count of
 *         items in each bin.
 *
 * Both output vectors are cleared and filled out by the histogram
 * function.
 */

#include <omp.h>
#include <stdio.h>
#include <iostream>
#include <cstddef>
#include <vector>
#include <algorithm> //std::min_element, std: max_element
#include <iterator> //begin and end
#include <cmath>
#include <math.h>

typedef std::vector<double> vec_t;
typedef std::vector<size_t> count_t;

void histogram(const vec_t &x, int num_bins,          /* inputs  */
                   vec_t &bin_bdry, count_t &bin_count) {  /* outputs */
	int n;
	int i; int j;
	double scale_factor; double bin_size;
	int bin;
	double mn = 1.0; double mx;
	int tid; int sum;
	int n_threads;
	n_threads = omp_get_max_threads();
	//printf("Number of threads is %d\n", n_threads);
	int matrix[n_threads][num_bins] = {};
	vec_t aa(num_bins+1);
	bin_bdry = aa;
	count_t aa2(num_bins);
	bin_count = aa2;
	n = x.size();
	#pragma omp parallel for reduction(min:mn)
	for (int i = 0; i<n; i++) {
        	if (x[i] < mn) mn = x[i];
}
	#pragma omp parallel for reduction(max:mx)
	for (int i = 0; i<n; i++)
        	if (x[i] > mx) mx = x[i];
	bin_bdry[0]=mn*(1-1E-14);
	bin_bdry[num_bins]=mx*(1+1E-14);
	
	#pragma omp parallel 
	{	
	bin_size = (mx-mn)/num_bins;
	#pragma omp for
	for (i = 1; i < num_bins; i++) {
		bin_bdry[i] = i*bin_size;
	}
	}
	scale_factor = bin_size;
	#pragma omp parallel shared(matrix) private(j, tid)   	
	{
	#pragma omp for 
	for (int j = 0; j < n; ++j) {
		const int tid = omp_get_thread_num();
			bin = std::ceil(x[j]/scale_factor) - 1;
			if (bin > num_bins-1){
				bin = num_bins-1;
				matrix[tid][bin] += 1;			}
			else {
				if (bin < 0) {
				bin = 0;
				matrix[tid][bin] += 1;
				}
				else {
			matrix[tid][bin] += 1;
			}
			}
	}
	#pragma omp for reduction(+:sum)
	for (int i = 0; i < num_bins; i++)
		 {
		sum = 0; bin_count[i]=0;
		for (j = 0; j < n_threads; j++) {
                	
                	sum += matrix[j][i];
		}
		bin_count[i] = sum;
	}
}
}
