#include <vector>
#include "sparse.h"

typedef double data_t;

/*
 *  *  Returns the shuffling index set such that v[idx] is sorted
 *   */
void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx){

    size_t n(0);
    idx.resize(v.size());
    std::generate(idx.begin(), idx.end(), [&]{ return n++; });
    std::sort(idx.begin(), idx.end(), 
              [&](size_t ii, size_t jj) { return v[ii] < v[jj]; } );
}

sparse::sparse(size_t m, size_t n, storage_type t) :
    m(m), n(n), type(t) {}

void sparse::transpose(sparse &B) const { //for now assume B is in COO format
	int rank;
	int nproc = 1;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);

	//Convert CSR format to COO format for input
	convert2COO(B);	

	vec_t index(B.row.size());
	index = B.row;
	sort_index(B.row, index);	

	//easy transpose in CSR format, switch row and column data
	vec_t temp = B.column;
	B.column = B.row;
	B.row = temp;	

	//initialize starting addresses for send buffers for each row, column, and data
	vec_t row_sendc(B.row.size());
	vec_t col_sendc(B.column.size());
	vec_t data_sendc(B.data.size());

	//counts_send, displace_send, counts_recv, and displace_recv need to be integer array of group size
	int *counts_send = new int[size];
	int *displace_send = new int[size];
	int *counts_recv = new int[size];
	int *displace_recv = new int[size];

	//keeping track of displacements
	int init_send_d = 0;
	int init_recv_d = 0;

	//the send and receive data types of buffer elements is MPI_INT
	MPI_Datatype type_send = MPI_INT;
	MPI_Datatype type_recv = MPI_INT;
	
	//Now need to fill send buffers
	for (int i = index[rank]; i < index[rank+1]; i++) {
		row_sendc[i] = B.row[i];
		col_sendc[i] = B.column[i];
		data_sendc[i] = B.data[i];
		counts_send[B.row[i]]++;
	}
	
	//Entry k specifies the displacement from which to take the outgoing
	//data destined for process k
	for (int j = 0; j < B.row.size(); j++) {
		int k = B.row[j]; 
		displace_send[k] = init_send_d++;
	}

	//need now to aggregate the number of elements being received from each process
	MPI_GATHER(&count_send, 1, type_send, counts_recv, 1, type_recv, MPI_COMM_WORLD);

	//Entry k specifies the displacement (relative to recvbuf) at which to place the incoming data from process k
	for (int k = 0; k < B.row.size(); k++)	{
		displace_recv[k] = init_recv_d;
		init_recv_d += counts_recv[k];
	}

	//initialize reveiving buffers for row, column, and data
	vec_t row_recvc(init_recv_d);
	vec_t col_recvc(init_recv_d);
	vec_t data_recvc(init_recv_d);

	//now need to Scatter an array and then gathering the array
	MPI_ScatterV(row_sendc, counts_send, displace_send, type_send, row_recvc,
			counts_recv, displace_recv, type_recv, MPI_COMM_WORLD);
	MPI_Gatherv(row_sendc, counts_send, displace_send, type_send, row_recvc,
                        counts_recv, displace_recv, type_recv, MPI_COMM_WORLD);
	MPI_Scatterv(col_sendc, counts_send, displace_send, type_send, col_recvc,
                        counts_recv, displace_recv, type_recv, MPI_COMM_WORLD);
	MPI_Gatherv(col_sendc, counts_send, displace_send, type_send, col_recvc,
                        counts_recv, displace_recv, type_recv, MPI_COMM_WORLD);
	MPI_Scatterv(data_sendc, counts_send, displace_send, type_send, data_recvc,
                        counts_recv, displace_recv, type_recv, MPI_COMM_WORLD);
	MPI_Gatherv(data_sendc, counts_send, displace_send, type_send, data_recvc,
                        counts_recv, displace_recv, type_recv, MPI_COMM_WORLD);
	
	//Set receiving buffers to B for output and delete stored integer arrays
	B.data = data_recv;
	B.column = col_recv;
	B.row = row_recv;

	delete[] counts_recv;
	delete[] displace_recv;
	delete[] counts_send;
	delete[] displace_send;

}

void sparse::convert2COO(sparse &B) const {
	B.type == sparse::COO;
	vec_t temp(B.data.size());
	for (int i = 0; i < B.row.size(); i++) {
		for (int j = B.row[i]; j < B.row[i+1]; j++)
			B.row[j] = i;
	}
}
