#include "transpose.h"

int main(int argc, char** argv) {
	MPI_Init(NULL,NULL);
	
	sparse B(4,4);
	B.row[] = {0,3,1,0};
	B.col[] = {0,3,1,2};
	B.data[] = {4,5,7,9};


        B.transpose(B);

	//print out transpose result data
	for (int i = 0; i < B.data.size(); i++) {
		std::cout << B.data[i] << " ";
	}
	std::cout<<"\n";
	
	//print row info
	for (int i = 0; i < B.row.size(); i++) {
                std::cout << B.row[i] << " ";
        }
        std::cout<<"\n";
	
	//print column info
	for (int i = 0; i < B.column.size(); i++) {
                std::cout << B.column[i] << " ";
        }
        std::cout<<"\n";

	MPI_Finalize();

	return 0;
}
