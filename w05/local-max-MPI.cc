
#include "slurp_file.h"
#include <mpi.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <array>
#include <string>
#include <vector>
#include <algorithm>

typedef int data_t;
typedef std::vector<data_t> vec_t;
using namespace std;

int find_max(vec_t v) {
	int mx = v[0];
	for (int i = 1; i < v.size(); i++) {
		if (v[i] > mx)
			mx = v[i];
	}
	return mx;	
}


int main(int argc, char** argv) {
	int rank, world_size;
	MPI_Init(NULL, NULL); //initialize MPI environment
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &world_size);	
	const char *filename="array.txt";
	vec_t data;
	vec_t local_max_array(4);
	int local_max;
	int global_max = 0;
//	MPI_Init(NULL, NULL); //initialize MPI environment
//	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
//	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	
//	for (int row = 0; row < 4; row++)
//	{
	//	vec_t data;
		if (rank == 0) {
		slurp_file_line(filename, rank, data);
 		for (auto i = data.begin(); i  != data.end(); ++i) 
			cout << *i << " ";
		cout << "\n";
		cout << "size of data row: " << data.size();
		cout << "\n";

		local_max = find_max(data);
		cout << "local_max for row: "<<rank<<" is "<<local_max << "\n";
		local_max_array[rank] = local_max;
		int b;
		MPI_Status* status;
		MPI_Recv(&b, 1, MPI_INT, 0,0,MPI_COMM_WORLD, status);
		if (local_max > b)
			global_max = local_max;
 		} else {
			slurp_file_line(filename, rank, data);
                for (auto i = data.begin(); i  != data.end(); ++i)
                        cout << *i << " ";
                cout << "\n";
                cout << "size of data row: " << data.size();
                cout << "\n";

                local_max = find_max(data);
                cout << "local_max for row: "<<rank<<" is "<<local_max << "\n";
                local_max_array[rank] = local_max;
		MPI_Send(&local_max, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
		}
				
		
	cout<<"Global max is: "<<global_max<<"\n";
//}
	MPI_Finalize();
}
