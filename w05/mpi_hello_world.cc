#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
	//need to initialize the MPI environment
	MPI_Init(NULL, NULL);

	//total number of processes
	int total;
	MPI_Comm_size(MPI_COMM_WORLD, &total);

	//get the id of the processor ie the rank
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	//result
	printf("Hello from processor %d of %d\n", rank, total);

	//need to finalize the MPI environment
	MPI_Finalize();
}
