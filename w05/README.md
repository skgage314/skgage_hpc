Task 1:

1. List all the options that mpicxx wrapper passes to C++ compilers.
        Answer: g++ -I/curc/sw/openmpi/1.10.2/gcc/6.1.0/include -pthread -L/usr/lib64 -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/curc/sw/openmpi/1.10.2/gcc/6.1.0/lib -Wl,--enable-new-dtags -L/curc/sw/openmpi/1.10.2/gcc/6.1.0/lib -lmpi_cxx -lmpi

Note to self: need to run source config.octance.rc to get correct modules

2. Please see mpi_hello_world.cc, it prints the correct information.

Task 2:

3/4 Was not able to extend my sequential code to include the MPI_Send and MPI_Recv in time, I am still working to fix this. My thinking is that every rank generates the local max for its respective line of the array.txt file. Only rank = 0 would recieve the local maxs from the other processors to then compute the global max. The other processors send their local max to processor 0. local_max.cc is the sequential code and local_max_MPI.cc is the current attempt with MPI implementation
