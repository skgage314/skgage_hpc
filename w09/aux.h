#include "poisson.h" 
#include <cmath>

constexpr double PI = 3.14159265358979323846;

double a_(double x, double y, double z);
double v_(double x, double y, double z);
double f_(double x, double y, double z);

void copy(const vec_t &source, vec_t &target);
void fill(const grid_t &x, vec_t &vec, std::function<double
		 (double, double, double)> func);
