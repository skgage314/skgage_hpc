/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief test driver for Possion
 */

#include "aux.h"
#include <iostream>

int main(int argc, char** argv){

	MPI_Init(&argc, &argv);
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// make sure input is correct
	if(argc != 2){
		if(world_rank == 0){
			printf("\nError: format is './test_poission.exe n', where");
			printf(" n is the number of\ndiscrete coordinates used to");
			printf(" describe each dimension after discretization.\n\n");
		}
		exit(1);
	}
	if(world_rank == 0){
		printf("%i threads with n = %i discretization points\n", 
				world_size, atoi(argv[1]));
	}

	// starting wall time
	real_t tic = MPI_Wtime();

    MPI_Comm grid_comm;
    int n = atoi(argv[1]); 
    grid_t x;
	// set up cartesian grid communicator
    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

    /** fill a and f based on x **/
    vec_t a, f;
	fill(x, a, a_);
	fill(x, f, f_); 
//	fill(x, diag, diag_);
    // make a matvec object to pass to the residual function (residual
    // doesn't care how you do the matvec, it just passes an input and
    // expect and output.
    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, 
			std::ref(a), std::placeholders::_1, std::placeholders::_2);

    // now you can call mv(v,lv)
    vec_t v, rhs, res;
    real_t res_norm;
	// fill v based on x
	fill(x, v, v_);
	// copy f vec into rhs as our reference for the residual
	copy(f, rhs); 
	// compute local residual
	residual(MPI_COMM_WORLD, mv, v, rhs, res, res_norm);

	// sum local residuals to get global residual
	real_t tot_res_norm = 0;
	real_t local_res_norm[world_size];
	// use MPI_Gather to get local value from all processors
	MPI_Gather(&res_norm, 1, MPI_DOUBLE, local_res_norm, 1, 
			   MPI_DOUBLE, 0, MPI_COMM_WORLD);
	if(world_rank == 0){
		// normalize residual w.r.t. total number of points, n^3 
		int N = n*n*n;
		for(int i(0); i < world_size; i++){
			tot_res_norm += local_res_norm[i]/N;
		}
		printf("Total residual norm: %g\n", tot_res_norm);
		// ending wall time
		real_t toc = MPI_Wtime();
		printf("Total time elapsed: %g seconds\n", toc-tic);
	}
	MPI_Comm comm;
	poisson_setup(MPI_COMM_WORLD, n, comm, x);
	vec_t diag(rhs.size()), u_final(rhs.size());
	int k = 0;
	int k_max = 100;
	for (int i = 0; i < f.size(); i++) {
		std::cout << "rhs = " << rhs[i] << " ";
		std::cout << "v = " << v[i] << " ";
	}
	jacobi_solver(comm, mv, diag, rhs, v, 1e-5, 1e-12, k_max, u_final, k);
	std::cout << "Total number of iterations: " << k << " ";
    /** cleanup **/
	MPI_Finalize();
    return 0;
}
