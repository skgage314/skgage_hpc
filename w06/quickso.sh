BATCH -i-time=0:05:00     # walltime, abbreviated by -t
#SBATCH --nodes=1          # number of cluster nodes, abbreviated by -N
#SBATCH -o histogram-%j.out     # name of the stdout redirection file, using the job$
#SBATCH -e histogram-%j.err     # name of the stderr redirection file
#SBATCH --ntasks 1      # number of parallel process
#SBATCH --qos debug        # quality of service/queue (See QOS section on CU RC$
#SBATCH --cpus-per-task=24
# run the program
echo "Running on $(hostname --fqdn)"
module load gcc

#for n in {1000000}
#do 
        #printf("\n");
#       printf("Histogram of a random array of size %d, num_bins=%d",n,num_bins);
  #      for num_bins in {1,3,4}
   #     do
                #printf("Histogram of a random array of size %d, num_bins=%d",n,num_bins); 
                for i in {1,2,4,8,12,16,20,24}
                do
                        OMP_NUM_THREADS=$i; ./test_quicksort.exe $((1000000))
                done
                echo " "
    #    done
#done
