#include "poisson.h"
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdio>
#include <math.h>
#include <cmath>
void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{
	/*
 *  * Set up the grid communicator for the Poisson problem and samples
 *   * the correct portion of the domain
 *    *
 *     * @comm input communicator, typically MPI_COMM_WORLD
 *      * @n problem size, there are n sample points along each axis
 *       * @grid_comm the grid communicator (output)
 *        * @x sample points assigned to the current MPI process based on
 *         *    its grid coordinates (output)
 *          */
	int world_size = 0;
	int world = MPI_Comm_size(comm, &world_size);
	if (world != MPI_SUCCESS)
		perror( "issue determining size of group associated with a communicator"); 
		//no error = MPI_SUCCESS,errors = MPI_ERR_COMM, OR MPI_ERR_ARG
	
	//Creating division of processors in a Cartesian grid
	int ndims = 3;
	int dims[ndims] = {0,0,0}; //input for MPI_Dims_Create and MPI_Cart_create
	world = MPI_Dims_create(world_size, 3, dims);
	if (world != MPI_SUCCESS)
		perror("issue creating MPI dimensions");

	//Making new communicator to which topology info has been attached
	//Inputs: comm_old, int ndims, int [] dims, array periods, int reorder, Output param: MPI_Comm comm_cart
	int periods[ndims] = {0,0,0};
	world = MPI_Cart_create(comm, ndims, dims, periods, 1, &grid_comm);
	if (world != MPI_SUCCESS)
        	perror("issue making new communicator with topology attached");
	
	//Set up MPI_Cart_rank - determines process rank in communicator given Cartesian location
	//Inputs: MPI_comm grid_comm, array coords of size ndims, Output param: int rank
	int rank = 0;
	int coords[ndims] = {0,0,0};
	MPI_Cart_rank(grid_comm, coords, &rank);

	//Set up MPI_Cart_coords - determines process corords in cartesian topology given rank in group
	//Inputs: MPI_Comm comm, int rank, int maxdims, Output param: int array coords of size ndims
	MPI_Cart_coords(grid_comm, rank, ndims, coords);

	//Need to assign sample points to current MPI process via grid_t x
	double m = 1/n; //normalize sampling points
	double starters[3] = {double(n/dims[0]), double(n/dims[1]), double(n/dims[2])};
	for (real_t xx = starters[0]*coords[0]*m; xx < (starters[0]*coords[0]+xx)*m; xx++) {
		for (real_t yy = starters[1]*coords[1]*m; yy < (starters[1]*coords[1]+yy)*m; yy++) {
			for (real_t zz = starters[2]*coords[2]*m; zz < (starters[2]*coords[2]+zz)*m; zz++) {
				point_t point = {xx, yy, zz};
				x.push_back(point);
			}
		}
	}
    //:std::cout<<"setup"<<std::endl;
}
int index(int x, int y, int z, int ix, int iy, int iz) {
	int i = x*ix*iy+y*iy+z;
	return i;
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{	
	/* @a value of coefficient a at points x
	 * 34 * @v candidate solution at points x
	 * 35 * @lv the matrix-vector product of the discrete Poisson’s operator
	*/
	
	//STILL WORKING ON THIS SECTION
	int index; // //need index of point of x in vectors a and v
	sample_pts = grid_comm.x;
	largest_pt = sample_pts.begin();
	smallest_pt = sample_pts.end();
	//compute distance between coordinates of the points
	int ix = largest_pt.x - smallest_pt.x;
	int iy = largest_pt.y - smallest_pt.y;
	int iz = largest_pt.z - smallest_pt.z;

	int index(int x, int y, int z) {
	        int i = x*ix*iy+y*iy+z;
        	return i;
	}
	vec_t lv(v.size());i
	MPI_STATUS status;
	for pt in sample_pts:
		i = pt.x;
		j = pt.y;
		k = pt.z;
		
		//need index of point of x in vectors a and v
		index = index(i, j, k);
		size_t v' = v[index];
		size_t a' = a[index];
		
		//depending on location in the cube, we send and recieve in different directions
		if (x == 0) { //far left in x dimension
			//int MPI_Cart_shift(MPI_Comm comm, int direction, int disp, int *rank_source,
			//                  int *rank_dest)
			int source, destination;
			//MPI_STATUS status;
			MPI_Cart_shift(grid_comm, 0, -1, &source, &destination); //displace downwards
			MPI_Send(v',1 MPI_DoUBLE, destination, 0, grid_comm);
			MPI_Cart_shift(grid_comm, 0, 1, &source, &destination); //displace upwards
			real_t buf; //initial address of recieve buffer
			int count = 1; //maximum number of elements in receive buffer (integer)
			MPI_Recv(&buf, count, MPI_DOUBLE, destination, 0, grid_comm, status);
			lv[index] = buf + v[index(i+1, j, k)] - 2v';
		} else if (i == ix - 1) {
			int source, destination;
                        //MPI_STATUS status;
                        MPI_Cart_shift(grid_comm, 0, 1, &source, &destination); //displace downwards
                        MPI_Send(v',1 MPI_DoUBLE, destination, 0, grid_comm);
                        MPI_Cart_shift(grid_comm, 0, -1, &source, &destination); //displace upwards
                        real_t buf; //initial address of recieve buffer
                        int count = 1; //maximum number of elements in receive buffer (integer)
                        MPI_Recv(&buf, count, MPI_DOUBLE, destination, 0, grid_comm, status);
                        lv[index] = buf - 2v' + v[index(i-1, j, k)];
		} else {
                        lv[index] = v[index(i+1, j, k)] - 2v' + v[index(i-1, j, k)];
		}
		if (y == 0) {
			int source, destination;
                        //MPI_STATUS status;
                        MPI_Cart_shift(grid_comm, 1, -1, &source, &destination); //displace downwards
                        MPI_Send(v',1 MPI_DoUBLE, destination, 0, grid_comm);
                        MPI_Cart_shift(grid_comm, 1, 1, &source, &destination); //displace upwards
                        real_t buf; //initial address of recieve buffer
                        int count = 1; //maximum number of elements in receive buffer (integer)
                        MPI_Recv(&buf, count, MPI_DOUBLE, destination, 0, grid_comm, status);
                        lv[index] = buf + v[index(i, j+1, k)] - 2v';
		} else if (y == iy - 1) {
			int source, destination;
                        //MPI_STATUS status;
                        MPI_Cart_shift(grid_comm, 1, 1, &source, &destination); //displace downwards
                        MPI_Send(v',1 MPI_DoUBLE, destination, 0, grid_comm);
                        MPI_Cart_shift(grid_comm, 1, -1, &source, &destination); //displace upwards
                        real_t buf; //initial address of recieve buffer
                        int count = 1; //maximum number of elements in receive buffer (integer)
                        MPI_Recv(&buf, count, MPI_DOUBLE, destination, 0, grid_comm, status);
                        lv[index] = buf - 2v' + v[index(i, j-1, k)];
		} else {
			lv[index] = v[index(i, j+1, k)] + v[index(i, j-1, k)] - 2v';
		}
		if (z == 0) {
			int source, destination;
                        //MPI_STATUS status;
                        MPI_Cart_shift(grid_comm, 2, -1, &source, &destination); //displace downwards
                        MPI_Send(v',1 MPI_DoUBLE, destination, 0, grid_comm);
                        MPI_Cart_shift(grid_comm, 2, 1, &source, &destination); //displace upwards
                        real_t buf; //initial address of recieve buffer
                        int count = 1; //maximum number of elements in receive buffer (integer)
                        MPI_Recv(&buf, count, MPI_DOUBLE, destination, 0, grid_comm, status);
                        lv[index] = buf + v[index(i, j, k+1)] - 2v';
		} else if (z == iz - 1) {
			int source, destination;
                        //MPI_STATUS status;
                        MPI_Cart_shift(grid_comm, 2, 1, &source, &destination); //displace downwards
                        MPI_Send(v',1 MPI_DoUBLE, destination, 0, grid_comm);
                        MPI_Cart_shift(grid_comm, 2, -1, &source, &destination); //displace upwards
                        real_t buf; //initial address of recieve buffer
                        int count = 1; //maximum number of elements in receive buffer (integer)
                        MPI_Recv(&buf, count, MPI_DOUBLE, destination, 0, grid_comm, status);
                        lv[index] = buf - 2v' + v[index(i, j, k-1)];
		} else { 
			lv[index] v[index(i, j, k+1)] + v[index(i, j, k-1)] - 2v';
		}
		lv[index] = a'*v' - lv[index]*(n-1);
		
			
			
}
	
void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs,
              vec_t &res, real_t &res_norm)
{
	/*
	* @comm mpi communicator
	* @mv the matvec operator
 	* @v the input vector to matvec
 	* @rhs the right-hand-side of the linear operator
 	* @res the point-wise residual (output)
 	* @res_norm the L2 norm of the residual vector ‘res‘
 	*/
	vec_t lv(rhs.size());
	poisson_matvec(comm, n, &mv, &v, &lv); 

	vec_t res(rhs.size());
        for (int i = 0; i < rhs.size(); i++) {
                res[i] = (rhs[i] - lv[i])
                res_norm += (rhs[i] - lv[i])*(rhs[i]-lv[i])
        }
	//std::cout<<"residual"<<std::endl;
}



