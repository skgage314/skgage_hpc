/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief test driver for Possion
 */

#include "poisson.h"
#include <iostream>

int main(int argc, char** argv){

    /** add initialization, get n from command line */

    MPI_Comm grid_comm;
    int n;
    grid_t x;
    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

    /** fill a and f based on x **/
    vec_t a, f;

    // make a matvec object to pass to the residual function (residual
    // doesn't care how you do the matvec, it just passes an input and
    // expect and output.
    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
                            std::placeholders::_1, std::placeholders::_2);

    // now you can call mv(v,lv)
    vec_t v, rhs, res;
    real_t res_norm;
    residual(MPI_COMM_WORLD, mv, v, rhs, res, res_norm);
    std::cout<<"Residual norm: "<<res_norm<<std::endl;

    /** cleanup **/
    return 0;
}
